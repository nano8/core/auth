<?php

namespace laylatichy\nano\core;

use DateTime;
use DateTimeZone;
use Exception;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use laylatichy\Nano;
use ReflectionClass;

final class Auth {
    private static Auth $_instance;

    private string      $secretShort;

    private string      $secretRefresh;

    private string      $issuer;

    private string      $audience;

    private string      $domain;

    private string      $algorithm;

    private int         $expireShort;

    private int         $expireRefresh;

    private int         $refreshRegenerate;

    private function __construct() {
    }

    public static function getInstance(): self {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();

            self::loadConfig();
        }

        return self::$_instance;
    }

    public static function getSecretShort(): string {
        return self::$_instance->secretShort;
    }

    public static function setSecretShort(string $secret): void {
        self::$_instance->secretShort = $secret;
    }

    public static function getSecretRefresh(): string {
        return self::$_instance->secretRefresh;
    }

    public static function setSecretRefresh(string $secret): void {
        self::$_instance->secretRefresh = $secret;
    }

    public static function getIssuer(): string {
        return self::$_instance->issuer;
    }

    public static function setIssuer(string $issuer): void {
        self::$_instance->issuer = $issuer;
    }

    public static function getAudience(): string {
        return self::$_instance->audience;
    }

    public static function setAudience(string $audience): void {
        self::$_instance->audience = $audience;
    }

    public static function getAlgorithm(): string {
        return self::$_instance->algorithm;
    }

    public static function setAlgorithm(string $algorithm): void {
        self::$_instance->algorithm = $algorithm;
    }

    public static function getExpireShort(): int {
        return self::$_instance->expireShort;
    }

    public static function setExpireShort(int $seconds): void {
        self::$_instance->expireShort = $seconds;
    }

    public static function getExpireRefresh(): int {
        return self::$_instance->expireRefresh;
    }

    public static function setExpireRefresh(int $seconds): void {
        self::$_instance->expireRefresh = $seconds;
    }

    public static function getRefreshRegenerate(): int {
        return self::$_instance->refreshRegenerate;
    }

    public static function setRefreshRegenerate(int $seconds): void {
        self::$_instance->refreshRegenerate = $seconds;
    }

    public static function generateShortToken(array $data = []): string {
        $now = time();

        $payload = [
            'iss' => self::getIssuer(),
            'aud' => self::getAudience(),
            'exp' => $now + self::getExpireShort(),
            'iat' => $now,
            'nbf' => $now,
            ...$data,
        ];

        return JWT::encode(payload: $payload, key: self::getSecretShort(), alg: self::getAlgorithm());

//        self::setCookie(nano: $nano, name: 'access-token', token: $token, expire: $payload['exp']);
    }

    public static function generateRefreshToken(Nano $nano, array $data = []): string {
        $now = time();

        $payload = [
            'iss'     => self::getIssuer(),
            'aud'     => self::getAudience(),
            'exp'     => $now + self::getExpireRefresh(),
            'iat'     => $now,
            'nbf'     => $now,
            'refresh' => $now + self::getRefreshRegenerate(),
            ...$data,
        ];

        $token = JWT::encode(payload: $payload, key: self::getSecretRefresh(), alg: self::getAlgorithm());

        self::setCookie(nano: $nano, name: 'session', token: $token, expire: $payload['exp']);

        return $token;
    }

    public static function validateShortToken(Nano $nano): ?object {
        try {
            $header = $nano->request->header(name: 'Authorization');

            if (!is_string($header)) {
                throw new NanoException(HttpCode::UNAUTHORIZED->name, HttpCode::UNAUTHORIZED->code());
            }

            $token = self::parseHeader(header: $header);

            return JWT::decode(
                jwt: $token,
                keyOrKeyArray: new Key(self::getSecretShort(), self::getAlgorithm())
            );
        } catch (Exception $e) {
            throw new NanoException(code: HttpCode::UNAUTHORIZED->code(), message: $e->getMessage());
        }
    }

    public static function validateRefreshToken(string $token): ?object {
        try {
            return JWT::decode(
                jwt: $token,
                keyOrKeyArray: new Key(self::getSecretRefresh(), self::getAlgorithm())
            );
        } catch (ExpiredException $e) {
            throw new ExpiredException(code: HttpCode::UNAUTHORIZED->code(), message: $e->getMessage());
        } catch (Exception $e) {
            throw new NanoException(code: HttpCode::UNAUTHORIZED->code(), message: $e->getMessage());
        }
    }

    public static function getDomain(): string {
        return self::$_instance->domain;
    }

    public static function setDomain(string $domain): void {
        self::$_instance->domain = $domain;
    }

    public static function destroyCookie(Nano $nano): void {
        $domain   = parse_url(url: self::getDomain(), component: PHP_URL_HOST);
        $expire   = time() - 86400;
        $dateTime = new DateTime();
        $dateTime->setTimestamp(timestamp: $expire);
        $dateTime->setTimezone(new DateTimeZone(timezone: 'UTC'));
        $format      = 'D, d M Y H:i:s e';
        $expiresText = $dateTime->format(format: $format);

        $nano->headers->add(
            header: 'Set-Cookie',
            value: "session=''; expires={$expiresText}; path=/; domain={$domain}; HttpOnly"
        );
//
//        $nano->headers->add(
//            header: 'Set-Cookie',
//            value: "access-token=''; expires={$expiresText}; path=/; domain={$domain}; HttpOnly"
//        );
    }

    public static function getTokenRaw(Nano $nano): ?string {
        $cookie = $nano->request->cookie(name: 'session');
        $header = $nano->request->header(name: 'Authorization');

        $token = null;

        if ($cookie && is_string($cookie)) {
            $token = $cookie;
        } elseif ($header && is_string($header)) {
            $token = self::parseHeader(header: $header);
        }

        return $token;
    }

    private static function loadConfig(): void {
        $file = Config::getRoot() . '/../.config/.auth.php';

        if (file_exists(filename: $file)) {
            require_once $file;

            self::checkIfInitialized();
        } else {
            throw new NanoException("no auth config file found ('.config/.auth.php')");
        }
    }

    private static function checkIfInitialized(): void {
        $reflection = new ReflectionClass(self::$_instance);

        foreach ($reflection->getProperties() as $property) {
            if (!$property->isInitialized(self::$_instance)) {
                throw new NanoException("property: {$property->getName()} not set");
            }
        }
    }

    private static function setCookie(Nano $nano, string $name, string $token, int $expire): void {
        $domain   = parse_url(url: self::getDomain(), component: PHP_URL_HOST);
        $dateTime = new DateTime();
        $dateTime->setTimestamp(timestamp: $expire);
        $dateTime->setTimezone(new DateTimeZone(timezone: 'UTC'));
        $format      = 'D, d M Y H:i:s e';
        $expiresText = $dateTime->format(format: $format);

        $nano->headers->add(
            header: 'Set-Cookie',
            value: "{$name}={$token}; expires={$expiresText}; path=/; domain={$domain}; HttpOnly"
        );
    }

    private static function parseHeader(string $header): string {
        return str_replace(search: 'Bearer ', replace: '', subject: $header);
    }
}
