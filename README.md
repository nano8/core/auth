### nano/core/auth

[![release](https://img.shields.io/packagist/v/laylatichy/nano-core-auth?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-auth)
[![PHPStan](https://img.shields.io/badge/PHPStan-Level%20max-brightgreen.svg?style=flat&logo=php)](https://shields.io/#/)
[![Total Downloads](https://img.shields.io/packagist/dt/laylatichy/nano-core-auth?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-auth)

##### CI STATUS

-   dev [![pipeline status](https://gitlab.com/nano8/core/auth/badges/dev/pipeline.svg)](https://gitlab.com/nano8/core/auth/-/commits/dev)
-   master [![pipeline status](https://gitlab.com/nano8/core/auth/badges/master/pipeline.svg)](https://gitlab.com/nano8/core/auth/-/commits/master)
-   release [![pipeline status](https://gitlab.com/nano8/core/auth/badges/release/pipeline.svg)](https://gitlab.com/nano8/core/auth/-/commits/release)

#### Install

-   `composer require laylatichy/nano-core-auth`
